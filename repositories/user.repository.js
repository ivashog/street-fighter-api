const fs = require('fs');
const fsPromises = fs.promises;
const path = require('path');

const FILE_PATH = path.resolve(__dirname + '/../db/users.csv') ;
const CSV_VALUE_DELIMITER = ',';
const CSV_ROW_DELIMITER = '\r\n';

const getOneById = async id => {
    if (!id) return null;

	const usersList = await getDataFromFile(FILE_PATH);
    const attributes = getAttributesNames(usersList);
	const index = findRowIndexById(id, usersList);

	if (index === -1) return null;

	const user = usersList[index].split(CSV_VALUE_DELIMITER);

	return user.reduce((response, value, idx) => {
		return { ...response, [attributes[idx]]: value };
	}, { });
};

const getAll = async () => {
	const usersList = await getDataFromFile(FILE_PATH);
	const attributes = getAttributesNames(usersList);

	let users = [ ];

	usersList.shift();
	usersList.forEach(row => {
		let user = { };

		row.split(CSV_VALUE_DELIMITER).forEach((value, idx) => {
			user[attributes[idx]] = value;
		});

		users.push(user);
	});

	return users;
};

const create = async user => {
	if (!user) return null;

	const usersList = await getDataFromFile(FILE_PATH);

	const newUser = {
		id: getLastUserId(usersList) + 1,
		...user
	};

	await appendDataToFile(newUser, FILE_PATH);

	return newUser;
};

const update = async (id, user) => {
	if (!id || !user) return null;

	const usersList = await getDataFromFile(FILE_PATH);
	const index = findRowIndexById(id, usersList);

	if (index === -1) return null;

	const existUser = usersList[index].split(CSV_VALUE_DELIMITER);
	const updatedUser = [
		existUser[0],
		...Object.values(user)
	].join(CSV_VALUE_DELIMITER);

	usersList[index] = updatedUser;
	const updatedData = usersList.join(CSV_ROW_DELIMITER);

	await fsPromises.writeFile(FILE_PATH, updatedData);

	return { id, ...user };
};

const dropByID = async id => {
	if (!id) return null;

	const usersList = await getDataFromFile(FILE_PATH);
	const index = findRowIndexById(id, usersList);

	if (index === -1) return null;

	usersList.splice(index, 1);
	const updatedData = usersList.join(CSV_ROW_DELIMITER);

	await fsPromises.writeFile(FILE_PATH, updatedData);

	return true;
}

const getDataFromFile = async path => {
	const usersFile = await fsPromises.readFile(path, {
		encoding: 'utf8'
	});

	return usersFile ? usersFile.split(CSV_ROW_DELIMITER): [ ];
};

const getLastUserId = usersList => {
	return usersList.length ?
		 +usersList[usersList.length - 1].split(CSV_VALUE_DELIMITER)[0] : 0;
};

const appendDataToFile = async (user, filepath) => {
	const dataCsv = Object.keys(user).reduce((csv, attr, idx) => {
		return csv += `${idx ? CSV_VALUE_DELIMITER : ''}${user[attr]}`;
	}, CSV_ROW_DELIMITER);
	return await fsPromises.appendFile(filepath, dataCsv, {
		encoding: 'utf8'
	});
};

const getAttributesNames = usersList => {
	return usersList[0].split(CSV_VALUE_DELIMITER);
};

const findRowIndexById = (id, usersList) => {
    const idsArr = usersList.map(row => row.split(CSV_VALUE_DELIMITER)[0]);
    return idsArr.findIndex(elem => +elem === +id);
};


module.exports = {
    getOneById,
	getAll,
    create,
	update,
	dropByID
};
