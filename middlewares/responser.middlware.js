module.exports = (req, res, next) => {
    res.success = (data, status = 200) => {
        if (!data) {
            return res.status(200).end();
        }
        return res.status(status).send({ data: data });
    };

    res.created = data => {
        return data ?
            res.status(201).send({ data: data }) :
            res.status(201).end();
    };

    res.noContent = () => {
        return res.status(204).end();
    };

    res.error = (message, status = 500) => {
        return res.status(status).send({
            error: status === 500 ?
                'Internal server error. ' + message :
                message
        });
    };

    res.badRequest = message => {
        return res.status(400).send({
            error: `Bad request${message ? '. ' + message : '.'}`
        });
    };

    res.unauthorized = () => {
        return res.status(401).send({
            error: 'Unauthorized.'
        });
    };

    res.forbidden = message => {
        return res.status(403).send({
            error: `Forbidden${message ? '. ' + message : '.'}`
        });
    };

    next();
};
