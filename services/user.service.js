const { create, getAll, getOneById, update, dropByID } = require("../repositories/user.repository");

const REQUIRED_USER_ATTRIBUTES = [ 'name', 'health', 'attack', 'defense' ];
const OPTIONAL_USER_ATTRIBUTES = [ 'gender', 'imgUrl' ];
// regexp used in type=”url” from RFC3986[https://www.ietf.org/rfc/rfc3986.txt]:
const URL_REGEXP = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/

const getById = async id => {
	validateUserId(id);

	return await getOneById(id);
};

const getListAll = async () => {
	return await getAll();
};

const createNewUser = async user => {
	if (!user) {
		throw new Error(`Bad request. Request body is empty!`);
	}
	validateOnRequiredAttributes(user);
	validateAttrValues(user);

    return await create(requestObjToDto(user));
};

const updateUser = async (id, user) => {
	validateUserId(id);

    if (!user) {
        throw new Error(`Bad request. Request body is empty!`);
	}

    validateOnRequiredAttributes(user);
    validateAttrValues(user);

    return await update(id, requestObjToDto(user));
};

const deleteUser = async id => {
	validateUserId(id);

	return await dropByID(id);
}

function requestObjToDto(user) {
    const {
        name, health, attack, defense, gender = '',
        imgUrl = 'http://lorempixel.com/240/320/people/'
    } = user;

    return { name, gender: gender.toUpperCase(), health, attack, defense, imgUrl };
}

function validateUserId(id) {
	if (id && Number.isNaN(+id)) {
        throw new Error(`Bad request. User ID must be a number!`);
	}
}

function validateOnRequiredAttributes(userObj) {
	const isValid = !REQUIRED_USER_ATTRIBUTES.some(attr => !Object.keys(userObj).includes(attr));
	if (!isValid) {
		throw new Error(`Validation error: attributes '${REQUIRED_USER_ATTRIBUTES}' is required!`);
	}
}

function validateAttrValues(userObj) {
	const { name, gender, imgUrl, ...numberAttributes } = userObj;

	if (
		typeof name !== 'string' ||
		name.length < 3 ||
		name.length > 100
	) {
		throw new Error(`Validation error: name must be a string with length 3...100 symbols!`);
	}

	if (gender) {
		if (
			typeof gender !== 'string' ||
			(gender.toUpperCase() !== 'M' &&
			gender.toUpperCase() !== 'F')
		) {
			throw new Error(`Validation error: gender must be a one char symbol ('m' or 'f')`);
		}
	}

	if (imgUrl) {
		const isValidUrl = URL_REGEXP.test(imgUrl + '');
		if (!isValidUrl) {
			throw new Error(`Validation error: url '${imgUrl}' is not valid!`);
		}
	}

	Object.keys(numberAttributes)
		.filter(attr => ['health', 'attack', 'defense'].includes(attr))
		.forEach(attr => {
			const isNumber = typeof +numberAttributes[attr] === 'number';
			if (!isNumber) {
				throw new Error(`Validation error: '${attr}' must be a number!`);
			}
		});

	const { health, attack, defense } = numberAttributes;

	if (+health < 10 || +health > 60) {
		throw new Error(`Validation error: 'health' must be in range [10...60]`);
	}

	[ attack, defense ].forEach(attr => {
		if (+attr < 1 || +attr > 5) {
			throw new Error(`Validation error: 'attack' and 'defense' must be in range [1...5]`);
		}
	})

}

module.exports = {
	getById,
    getListAll,
    createNewUser,
	updateUser,
	deleteUser
};
