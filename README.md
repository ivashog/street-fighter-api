# [Street Fighters API](https://binary-studio-academy.github.io/stage-2/lectures/express-yourself-with-nodejs/)

## How to

#### Requirements

*You must have instalig **Node.js 10.0.0** or higher on your machin, becouse in this project using the fs.promises API is experimental from version 10*

#### How to start project

1. `git clone git@bitbucket.org:ivashog/street-fighter-api.git`
2. `cd street-fighter-api`
3. `npm i`
4. `npm run start` or `nodemon` (if he install global)
5. By default server running on [localhost:3000](http://localhost:3000)


## Features

#### Arhitecture

Monolit with following layers:

- Routes
- Services (Main BL)
- Repositories (working with file db)
- Middelwares (auth and responser)

#### Users(fighters) CRUD

RESTful API that can handle the list of following requests:

- **GET**: _/user_
  get an array of all users

- **GET**: _/user/:id_
  get one user by ID

- **POST**: _/user_
  create user from the request body data

- **PUT**: _/user/:id_
  update user from the request body data and ID param

- **DELETE**: _/user/:id_
  delete one user by ID

#### Authorization for create, update and delete routes

For autorize add `Authorization` header with value `admin`
