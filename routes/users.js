const express = require('express');
const router = express.Router();

const { getById, getListAll, createNewUser, updateUser, deleteUser } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get('/:id', async (req, res, next) => {
    try {
		const userId = req.params.id;
		const user = await getById(userId);

		if (user === null) {
			return res.badRequest(`User with id=${userId} not found`);
		}

		return res.success(user);
	} catch (err) {
		console.error(err);
		return res.error(err.message);
	}
});

router.get('/', async (req, res, next) => {
    try {
		const users = await getListAll();

		return res.success(users);
	} catch (err) {
		console.error(err);
		return res.error(err.message);
	}
});

router.post('/', isAuthorized, async (req, res, next) => {
    const user = req.body;

    if (!user) return res.badRequest('request body is empty');

    try {
	  	const createdUser = await createNewUser(user);

	  	return res.created(createdUser);
    } catch (err) {
    	return res.error(err.message, 400);
    }
});

router.put('/:id', isAuthorized, async (req, res, next) => {
    const userId = req.params.id;
    const user = req.body;

    if (!user) return res.badRequest('request body is empty');

    try {
	  	const updatedUser = await updateUser(userId, user);

	  	return res.success(updatedUser);
    } catch (err) {
    	return res.error(err.message, 400);
    }
});

router.delete('/:id', isAuthorized, async (req, res, next) => {
    try {
		const userId = req.params.id;
		const isDeleted = await deleteUser(userId);

		if (!isDeleted) return res.badRequest(`User with id=${userId} not found`);

	  	return res.noContent();
    } catch (err) {
    	return res.error(err.message, 400);
    }
});

module.exports = router;
